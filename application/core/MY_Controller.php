<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();
        // do some stuff
        $GLOBALS['userData'] = array();
        global $userData;
        $userData['controller'] = $this->uri->segment(2);
        $this->load->model('m_global');
    }

    public function index() {

        $this->view();
    }

    public function view() {
        global $userData;
        $controller = $userData['controller'];
        $data['title'] = "$controller Manager";
        $data['header'] = "View $controller";
        $data['contend'] = "backend/" . $controller . "/view";
        $get_data = $this->get_data();
        $data['userData'] = $get_data['recode'];
        pagination_config(base_url() . 'admin/' . $controller . '/view', $get_data['num_rows']);
        $userData['pagination_link'] = $this->pagination->create_links();

        $this->load->view('backend/template', $data); // dynamic view
    }

    function get_data() {
        global $userData;
        if (isset($userData['jiontTable']) && $userData['jiontTable'] != "") {

            return $this->m_global->m_select_join($userData['jiontTable']);
        } else {
            return $this->m_global->m_select($userData['table']);
        }
    }

    function get_data_edit($id) {
        global $userData;
        $table = $userData['table'];
        $where = array($userData['pk'], $id);
        return $this->m_global->m_select_where($table, $where);
    }

    function get_dropdown($tablesDropdown) {
        global $userData;
        foreach ($tablesDropdown as $dropdownList) {
            $table = $dropdownList[0];
            $data[$table . "_list"] = $this->m_global->getDataArray($table, $dropdownList[1], $dropdownList[2]);
        }
        return $data;
    }

    public function add() {
        global $userData;
        $controller = $userData['controller'];
        $userData['action'] = $controller."/insertData";
        if (isset($userData['dropTable']) && $userData['dropTable'] != "") {
            $data = $this->get_dropdown($userData['dropTable']);
        }
        $data['title'] = "$controller Manager";
        $data['header'] = "Manage $controller";
        $data['contend'] = "backend/$controller/add";
        $this->load->view('backend/template', $data); // dynamic view
    }

    public function edit($pk = null) {
        $this->clear_session('pk'); // user old edit session
        global $userData;
        $controller = $userData['controller'];
        $userData['action'] = $controller . "/updateData";

        if (isset($userData['dropTable']) && $userData['dropTable'] != "") {
            $data = $this->get_dropdown($userData['dropTable']);
        }
        $data['userData'] = $this->get_data_edit($pk);
        //write id for update data
        $this->session->set_userdata('pk', $pk);

        $data['title'] = "$controller Manager";
        $data['header'] = "Edit $controller";
        $data['contend'] = "backend/" . $controller . "/edit";
        $this->load->view('backend/template', $data); // dynamic view
    }

/// insert recode to database according to the user input fiealds
    public function insertData() {
        global $userData;
        $controller = $userData['controller'];
        $table = $userData['table'];
        $add_data = $this->m_global->m_add($table);
        if ($add_data) {
            $this->alert(TRUE, "Success add new record..!");
        } else {
            $this->alert(FALSE, "Error add a record..!");
        }

        redirect(base_url() . 'admin/' . $controller);
    }

/// update recode to database according to the user edite session key
    public function updateData() {
//        echo         $this->session->userdata('pk'); exit();
        global $userData;
        $controller = $userData['controller'];
        $table = $userData['table'];
        $where = array($userData['pk'], $this->session->userdata('pk'));

        $edit_data = $this->m_global->m_edit($table, $this->input->post(), $where);
        if ($edit_data) {
            $this->alert(TRUE, "Success update record..!");
        } else {
            $this->alert(FALSE, "Error update a record..!");
        }

        redirect(base_url() . 'admin/' . $controller);
    }

    // delete data by premarikey 
    function delete($pk) {
        global $userData;
        $controller = $userData['controller'];
        $table = $userData['table'];
        $where = array($userData['pk'], $pk);
        $delete_data = $this->m_global->m_delete($table, $where);
        if ($delete_data) {
            $this->alert(TRUE, "Success delete a record from database..!");
        } else {
            $this->alert(FALSE, "Error delete a record..!");
        }

        redirect(base_url() . 'admin/' . $controller);
    }

    function validate_form() {
        $this->form_validation->set_rules('firstname', 'first name', 'required|min_length[5]');
        $this->form_validation->set_rules('lastname', 'last name', 'required|min_length[5]');
        $this->form_validation->set_rules('sex', 'gander', 'required');
        $result = $this->form_validation->run();

        if ($result) {
            echo "Success...!";
            exit();
        }
        $this->edit();
    }

    function alert($type, $sms) {
        $this->session->set_flashdata('sms', $sms);
        $aletType = "danger";
        if ($type) {
            $aletType = "success";
        }
        $this->session->set_flashdata('alertType', $aletType);
    }

    function clear_session($name) {
        if ($this->session->userdata($name) != NULL) {
            $this->session->set_userdata($name, "");
        }
    }

}
