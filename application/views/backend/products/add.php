
<?php

echo start_myform();
$form_elements = array(
    array('label' => 'Product Name', 'name' => 'pname'),
    array('label' => 'Supplier', 'name' => 'supplier'),
    array('label' => 'Unitprice', 'name' => 'unitprice'),
    array('label' => 'Store', 'name' => 'pro_storeid', 'items' => $store_list, 'type' => 'dropdown'),
    array('label' => 'Category', 'name' => 'pro_catid', 'items' => $category_list, 'type' => 'dropdown')
);

echo form_items($form_elements);

echo close_myform();



