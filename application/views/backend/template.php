<?php
$alertType = $this->session->flashdata('alertType');
$sms = $this->session->flashdata('sms');
?>
<!DOCTYPE html>
<html>

    <title><?php echo $title; ?></title>
    <script src="<?php echo base_url(); ?>assets/jquery-1.9.1.js" ></script>
    <script src="<?php echo base_url(); ?>assets/list.js" ></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <link href="<?php echo base_url(); ?>assets/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="<?php echo base_url(); ?>assets/style.css" rel="stylesheet" type="text/css" media="all">
</head>
<body>

    <div class="container">
        <legend class="header">
            <h3 class="pull"><?php echo $header ?></h3>
        </legend>

        <?php
        if($sms !=""){ ?>
        <div class="alert alert-<?php echo $alertType ?> alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $sms; ?>
        </div>
        
        <?php } ?>
        
        <?php
        $this->load->view($contend); // load contain of page
        ?>    
    </div> <!-- /container -->

</body>