<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends MY_Controller {

    function __construct() {
        parent::__construct();
        global $userData;
        $userData['table']='product'; //table name
        $userData['pk']='pid'; //you need to input primary key of the table
        
        // initail dropdown input for add new / update form
         $userData['dropTable'] =array(
            array ('category','catid','catname'),  //the system will create dropdown list named category_list
            array ('store','storeid','storename'));//the system will create dropdown list named store_list
         
        //////====== initial join table for view
       $userData['jiontTable']= array('product',
           array('category','pro_catid = catid'), //join table mush user disfference column name
           array('store','pro_storeid = storeid')
       );
        
    }

}
