<?php

class M_global extends CI_Model {

    /**
     * Function Execute mysql query and return an array
     * @param string $query to execute 
     * @return array list
     */
    function get_array_data($query = null) {
        global $connection;
        $result = mysqli_query($connection, $query);
        $rows = '';
//    var_dump($result);exit();
        if ($result && mysqli_num_rows($result) > 0) {
            while ($get_result_to_array = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                $rows[] = $get_result_to_array;
            }
        }
        return $rows;
    }

    /**
     * Select record from a table
     * @param string $query to execute 
     * @return array list
     */
    function m_select($table) { // return array result
        $num_page = RECODE_PERPAGE; // limit number of recode for each page
        $start = $this->uri->segment(4); //get start recode to select from
        $this->db->limit($num_page, $start);
        $result = $this->db->get($table);
         
        if ($result != Null) {
            $data['recode']= $result->result_array();
            $data['num_rows']= $this->record_count($table);
            return $data;
        }
        return FALSE;
    }
    

    function m_select_where($table, $where = null) { // return array result
        $this->db->where($where[0], $where[1]);
        $result = $this->db->get($table);

        if ($result != Null) {
            $data = $result->result_array();
            
            return $data[0];
        }
        return FALSE;
    }

//$tables = array(
//    array('tbl1'=>'tbl_food','tb2'=> 'tbl_category','where_case'=>'foo_cat_id=cat_id','join'=>'inner join'),
//     array('tbl1'=>'tbl_food','tb2'=> 'tbl_food_type','where_case'=>'foo_type_id=type_id','join'=>'inner join')
//);

    function m_select_join($tables, $where = NUll) {
        $data="";
        $num_page = RECODE_PERPAGE; // limit number of recode for each page
        $start = $this->uri->segment(4); //get start recode to select from
        $result = NULL;
        $this->db->from($tables[0]);
        $i = 0;
        foreach ($tables as $table) {
            if ($i > 0) {
                $this->db->join($table[0], $table[1], 'left');
            }
            $i++;
        }
        
        $this->db->limit($num_page, $start);
        $result = $this->db->get();

        if ($result != Null) {
            $data['recode']= $result->result_array();
            $data['num_rows'] =$this->record_count_join_table($tables);
            return $data;
        }
        return FALSE;
    }

    function m_search() {
        
    }

// add new recode to database with dynamic table and fields
    function m_add($table) { // get $POST
        $result = $this->db->insert($table, $this->input->post());
        return $result;
    }

    function m_edit($table, $data, $where = null) { // Need dimationall array for where case
        $this->db->where($where[0], $where[1]);
        $result = $this->db->update($table, $data);
        return $result; // return True/ False to caller function
    }

// delete data follow premarykey
    function m_delete($table, $where = null) {
        $this->db->where($where[0], $where[1]);
        $result = $this->db->delete($table);
        return $result; // return True/ False to caller function
    }

// Writ alet message on the top of the user inter face
    function alert($type, $sms = NULL) {
        if ($type) { // get the alert success
            $_SESSION['sms'] = $sms;
            $_SESSION['get_style'] = 'success';
        } else { // get the alert unsuccess
            $_SESSION['get_style'] = 'danger';
            $_SESSION['sms'] = $sms;
        }
        $_SESSION['alert'] = TRUE; //passing message to UI to show alert box
    }

    /**
     * 
     * @param string $table_name
     * @param string $field_key
     * @param string $field_value
     * @param string $field_status
     * @param int $status_value 1 or 0
     * @return array
     */
    function getDataArray($table_name, $field_key, $field_value, $field_status = null, $status_value = 1) {
        $this->db->select($field_key . ',' . $field_value);
        if ($field_status != NULL)
            $this->db->where($field_status, $status_value);
        $this->db->from($table_name);
        $data = $this->db->get();
        $result = array();
        if ($data->num_rows() > 0) {
            foreach ($data->result_array() as $row) {
                $result[$row[$field_key]] = $row[$field_value];
            }
        }
        return $result;
    }

    public function record_count($table) {
        return $this->db->count_all($table);
    }
    function record_count_join_table($tables){
        $this->db->from($tables[0]);
        $i = 0;
        foreach ($tables as $table) {
            if ($i > 0) {
                $this->db->join($table[0], $table[1], 'left');
            }
            $i++;
        }
        $result = $this->db->get();
        return $result->num_rows();
        
    }

}
